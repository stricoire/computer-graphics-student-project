# Computer graphics and Image Processing Student Project

This a compilation of <b>Computer Graphics</b> and <b>Image Processing</b> projects, I made during my Master's degree in Computer Science.<br>
In this repository you'll find complete access to my source code.

Here after, I present only a fraction of it.

## Recursive ray tracer implementation

<figure>
  <img src="Pictures/computerGraphics_rayTracing.png"
       width="500">
</figure>

<br>

In computer graphics, ray tracing is a rendering technique that generates an image <br>
by tracing a ray from the camera through each pixel in the scene, which will then, display the nearest object.
<br> <br>This project was realized as part of a Computer Graphics course.
<br> <b>(C++ / Opengl)</b>
<br> (2019)


## Deferred Shading implementation

<figure>
  <img src="Pictures/computerGraphics_deferredShading.png"
       width="500">
</figure>

<br>

The deferred shading is a rendering technique that separate the calculation from the visibility, of the lighting calculation. In this way, the calculations and memory access needed to shade the scene are reduced to only the visible portions.
<br> <br>This project was realized as part of a Computer Graphics course.
<br> <b>(C++ / Opengl)</b>
<br> (2019)


## Normal mapping implementation

<figure>
  <img src="Pictures/computerGraphics_normalMapping.png"
       width="500">
</figure>

<br>

The principle behind this approach is to store in a texture map, detailed normals that will be used to disrupt the lighting calculation. It is used to graphically simulate geometric details, without adding polygons to the real geometry.
<br> <br>This project was realized as part of a Computer Graphics course.
<br> <b>(C++ / Opengl)</b>
<br> (2019)


## Virtual reality tracking implementation

<figure>
  <img src="Pictures/carousel_VR.png"
       width="500">
</figure>

<br>

We were interested in visual immersion .
So we reproduced the perception of depth from the projection of the 3D environment in two dimensions,
to facilitate the manipulation of 3D objects and navigation. We finally added head tracking.
<br> <br>This project was realized as part of a VR course.
<br> <b>(JS / Three.js)</b>
<br> (2019)


## Transferring color to greyscale

<figure>
  <img src="Pictures/imageProcessing_transferringColorToGreyscale.png"
       width="500">
</figure>

<br>

We used this scientific paper (<i>here only the first page</i>) to develop an algorithm
to transfer color from one image to another one in greyscale.
<br> <br>This project was realized as part of an Image Processing course.
<br> <b>( C language )</b>
<br> (2019)


## Color transfer

<figure>
  <img src="Pictures/imageProcessing_colorTransfer.png"
       width="500">
</figure>

<br>

We used this scientific paper (<i>here only one page</i>) to develop an algorithm
to transfer color from one image to another one based on statistical analysis.
<br> <br>This project was realized as part of an Image Processing course.
<br> <b>(C language)</b>
<br> (2019)


## Other Image Processing projects

Feel free to check all the other projects we worked on, with result : [ImageProcessing_results](Pictures/ImageProcessing_results.pdf)
